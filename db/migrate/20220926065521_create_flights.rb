class CreateFlights < ActiveRecord::Migration[7.0]
  def change
    create_table :flights, id: :uuid do |t|
      t.string :name, null: false
      t.integer :generation, null: false
      t.datetime :start_time, null: false
      t.datetime :end_time, null: false
      t.decimal :latitude, null: false
      t.decimal :longitude, null: false

      t.timestamps
    end
  end
end
