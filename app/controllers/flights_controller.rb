class FlightsController < ApplicationController
  before_action :set_flight, only: %i[ show update destroy ]

  # TODO: Add Serializers for cleaner JSON renders
  # GET /flights
  def index
    @flights = Flight.all

    unless params.nil?
      # TODO: Handle incorrect queries, should return error than
      # default Flight.all
      if params[:name]
        @flights = @flights.where(name: params[:name])
      end

      if params[:generation]
        @flights = @flights.where(generation: params[:generation])
      end

      # TODO: Able to filter if only suppled one date ref
      if params[:start_time] && params[:date_time]
        @flights = @flights.where(generation: params[:generation])

        if params[:interval]
          @flights = @flights.where(generation: params[:generation])
        end
      end
    end

    render json: @flights
  end

  # GET /flights/1
  def show
    render json: @flight
  end

  # POST /flights
  def create
    if params[:flights]
      @flights = Flight.insert_all!(params[:flights])

      render json: "Successful inserted #{@flights.count} flight logs", status: :created, location: @flight
    else
      @flight = Flight.new(flight_params)

      if @flight.save
        render json: @flight, status: :created, location: @flight
      else
        render json: @flight.errors, status: :unprocessable_entity
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_flight
      @flight = Flight.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def flight_params
      params.permit(:name, :generation, :start_time, :end_time, :latitude, :longitude)
    end
end
