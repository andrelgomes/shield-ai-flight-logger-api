sequenceDiagram
    autonumber
    Device->>Server Instance
    Server Instance->> Federated PostgreSQL
    Data warehouse
    Redis
    GraphQL
    External Data Processing
