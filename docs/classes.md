classDiagram
    Flight  : +UUID uuid
    Flight  : +String robot_name
    Flight  : +Int generation
    Flight  : +Decimal latitude
    Flight  : +Decimal longitude
    Flight  : +Datetime start_time
    Flight  : +Datetime end_time
