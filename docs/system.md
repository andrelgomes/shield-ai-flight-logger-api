sequenceDiagram
    autonumber
    Device->>Rails Server
    Rails Server->>PostgreSQL
    POST /flights/ 
    GET /flights/:id
    GET /flights/:id?generation=&start_time=&end_time=&interval=&
