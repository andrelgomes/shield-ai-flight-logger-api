---
author: Andre L Gomes
---
# Drone Flight Log API

## Requirements
* Store every flight, acessible across company
* Handle individual inserts and bulk inserts
* Retrieve logs with filtering parameters
    * flight logs for drone generation 16
    * date range
    * shorter than 15 minutes
* Invalid queries should have proper error handling and respond with an appropriate error.
* Logs should be persisted with consideration towards efficiency of storage and search

Flight Log
* Robot: a unique device name, string
* Device Generation: product line/ model number (e.g. 1-26)
* Start Time (UTC)
* End Time (UTC)
* Geo Location (Lat/Lon)

## How to run
```bash
bundle install
rails db:create
rails db:migrate
```

## Postman

POST /flights/ 
GET /flights/:id
GET /flights/:id?generation=&start_time=&end_time=&interval=&

## Documentation

[Basic Architecture](./docs/system.md)
[Advance Architecture](./docs/advance-system.md)
[Classes](./docs/classes.md)

## Additional Questions
* Classifies a Robot/Drone uniquness?
