require "test_helper"

class FlightsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @flight = flights(:one)
  end

  test "should get index" do
    get flights_url, as: :json
    assert_response :success
  end

  test "should create flight" do
    assert_difference("Flight.count") do
      post flights_url, params: { flight: { end_time: @flight.end_time, generation: @flight.generation, latitude: @flight.latitude, longitude: @flight.longitude, name: @flight.name, start_time: @flight.start_time } }, as: :json
    end

    assert_response :created
  end

  test "should show flight" do
    get flight_url(@flight), as: :json
    assert_response :success
  end

  test "should update flight" do
    patch flight_url(@flight), params: { flight: { end_time: @flight.end_time, generation: @flight.generation, latitude: @flight.latitude, longitude: @flight.longitude, name: @flight.name, start_time: @flight.start_time } }, as: :json
    assert_response :success
  end

  test "should destroy flight" do
    assert_difference("Flight.count", -1) do
      delete flight_url(@flight), as: :json
    end

    assert_response :no_content
  end
end
